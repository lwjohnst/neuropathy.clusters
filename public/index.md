---
title: "A data-driven approach at characterizing heterogeneity in neuropathy assessments"
date: "March 23 2020"
site: bookdown::bookdown_site
documentclass: book
bibliography: [resources/refs.bib, resources/packages.bib]
link-citations: yes
---

# Manuscript


![](https://img.shields.io/badge/document%20status-rough%20draft-red?style=flat-square)



Contributing instructions
-------------------------

For referencing:

-   If you use bibtex (.bib files) for your reference manager, copy and
    paste the bibkey for the reference into the place you want to cite.
    Then send me your .bib file so I can update the references in the
    document.

-   If you don't use bibtex, for any references you want to cite, put
    the article DOI in the place that you would normally cite in the
    text. I will add the appropriate reference from that point.

To see the results, go to the [[Results
webpage]{.underline}](https://lwjohnst.gitlab.io/neuropathy.clusters/results.html).
If you see things like **[@Smith2000]**, this is the citation key
that will be used when generating the reference list. Don't remove it.

**TODO List** (also found
[[here]{.underline}](https://gitlab.com/lwjohnst/neuropathy.clusters/issues/73);
cross out when completed):

-   Draft of non-analysis sections in methods (*Lasse*)

-   Draft of background section (*Signe*)

-   Review/update on literature for relevant studies (*Lasse & Signe*)

-   Complete figures and tables of results (*Luke*)

-   Draft of results section (*Luke*)

-   Draft of EASD abstract (*Signe*). **Deadline**: April 1

-   Proofread working draft of manuscript (*Luke*)

-   Send a working draft to Daniel for review. **Deadline**: April 15

-   Send draft for review to co-authors. **Deadline**: April 30

*Add any other Contributing Guidelines here.*

Background
----------

Diabetic peripheral neuropathy (DPN) is a common complication of
diabetes usually presenting as sensory loss, pain and weakness in the
lower limbs due to distal to proximal progression of damage to different
nerve fiber populations. Yet, symptoms can present in very different
ways, often fluctuate and can go unnoticed by the patient while
objective measures show clear nerve dysfunctions. Consequently, no
robust diagnostic definition or gold standard assessment exists that
fully encompasses the heterogeneity and changing course of nerve fiber
dysfunctions seen in the course of DPN.

The heterogeneity of DPN has resulted in the development of a wide range
of assessments by various diagnostic tools to register the presence of
symptoms (generally based on questionnaires) and to quantify the
different potentially affected modalities (e.g. sensation of vibration,
light touch, temperature, sharp pain etc.) by clinical tests. These
tests are complicated by sometimes marked age-related decline in
neuronal function. Current definitions of DPN range from purely
questionnaire-based definitions to definitions that integrate clinical
signs and symptoms of DPN in various ways[REF: different definitions].
These various DPN assessments and definitions have typically been
validated against nerve conduction studies, considered the most specific
clinical assessments of DPN; yet also using varying criteria.
Consequently, a mix of age-specific- and more or less arbitrary cut-offs
are used to evaluate the presence of DPN based on the different
assessments.

At present, the most accepted definition for DPN is a hierarchical
probability-classification into possible, probable and definite DPN as
proposed by the Toronto consensus criteria (DOI:
[[10.2337/dc10-1303]{.underline}](https://doi.org/10.2337/dc10-1303)).
These criteria combine various symptoms, simple clinical signs and more
advanced confirmatory assessments of nerve fiber dysfunction. While
these criteria allow for the changing course of nerve fiber dysfunction
in DPN, they also illustrate the lack of a simple definition of DPN that
is valid throughout the course of this disease.

The clinical 13-year follow-up examination of the ADDITION-Denmark study
examined 452 people with type 2 diabetes identified through screening at
the study baseline. It found a prevalence of definite DPN of 27% while
another 59% had either possible or probable DPN (DOI:
[[10.2337/dc18-0392]{.underline}](https://doi.org/10.2337/dc18-0392))
according to the Toronto criteria. This clearly illustrates that using
different assessments to diagnose DPN in the same population is
problematic and identifies different subsets of people with DPN.

There is a need for a more accurate definition of DPN not relying on
arbitrary evaluations of different assessments of nerve fiber
dysfunction.

Few (or no) studies to date have.....

Therefore, we............used a data-driven approach to identify
heterogeneity in responses across a wide set of neuropathy assessments
performed in the clinical 13-year follow-up examination of
ADDITION-Denmark .

The most used tools include: LIST CURRENT TOOLS WITH REFERENCES. Maybe
just describes this in the methods - there is no simple list of most
common/current tools.

TODO: Fix up, taken from abstract.

TODO: Add references where needed.

### Objectives

Our overall aim was to identify heterogeneity in characteristics of
neuropathy in people with type 2 diabetes as determined by the
assessment items[^1] and responses. Specifically, we approached this
heterogeneity by using a wide range of neuropathy assessment items
across multiple assessment tools[^2] to identify potential subgroups of
individuals in their responses and in potentially underlying factors in
the assessment items.

Methods
-------

### Study population

We conducted a cross-sectional analysis of the Danish arm of the
Anglo-Danish-Dutch study of Intensive Treatmentin People with
Screen-Detected Diabe-tes in Primary Care (ADDITION), which has been
previously described {{ref}}. Briefly, between 2001 to 2006 {{correct?}}
ADDITION-Denmark enrolled patients with previously undiagnosed diabetes
who were aged 40--69 years through a stepwise screening program from
primary care clinics. Patients completed a self-administered risk score
questionnaire. The exclusion criteria was those who had previously
diagnosed diabetes, were pregnant or lactating, were housebound, had a
life expectancy of less than a year, or were unable to provide informed
consent.

There were 1533 participants who were eventually enrolled in
ADDITION-Denmark from 190 participating general practices. General
practices were randomized to deliver either the routine care for
diabetes or to provide an intensive multifactorial target-driven care
until the trial was concluded in 2009. After the study conclusion,
patients were to subsequently follow the current guidelines for diabetes
care. Since the closure of ADDITION, participants have been followed up
by filling out questionnaires, tracked through the Danish healthcare
registers, and have completed a clinical follow-up examination after 13
years of follow up in 2015-2016.

### Neuropathy assessments

Diabetic neuropathy was assessed using different clinical scoring
systems (TCSS, mTCSS, UENS, MNSI, DN4), Sural nerve conduction studies,
and heart rate variability (HRV) measurements (n>430 completed all
assessments).

All assessments were conducted at the 13-year followup. {{Correct?}}

-   **MNSI**: The Michigan Neuropathy Screening Instrument (MNSI) was
    developed as a screening tool to assess diabetic peripheral
    neuropathy (DPN) {{ref}} and contains a 15-item self-administered
    questionnaire. It has been validated {{against what?}} for the
    diagnosis of DPN (24). {{Other?}}

-   **TCSS**: The Toronto Clinical Scoring System (TCSS) was developed
    as a scoring system that evaluates neuropathic symptoms, ankle- and
    knee reflexes, and sensory testing of the first toe. Besides light
    touch and pin prick the sensory testing includes vibration-,
    temperature-, and position sensation. It has been validated for the
    diagnosis of diabetic sensorimotor neuropathy (bril V et al.,
    diabetes care 2002, PMID: 12401755, DOI: 10.2337)

-   **mTCSS**: The modified Toronto Clinical Scoring System (mTCSS) came
    in 2009 (bril V et al, diabetic medicine 2009, PMID: 19317818, DOI
    10.1111). The mTCSS is a modification of the original scoring system
    that leaves out measurement of tendon reflexes. This scoring system
    has been evaluated with seven different neuropathy scoring systems
    in people with impaired glucose tolerance and had the highest
    detection accuracy for DPN (Zilliox LA et. al journal of diabetes
    complications 2015, PMID: 25690405, DOI: 10.1016.

-   **DN4**: The Douleur Neuropathique 4 (DN4) is a screening tool for
    painful diabetic peripheral neuropathy. It was developed in 2005 and
    it includes three physical examination items supplemented with seven
    symptom questions (Bouhassira, Pain, PMID: 15733628, DOI: 10.1016

-   **Sural nerve conduction**: Is a measure of distal peripheral
    neuropathy (DPN)

-   **Heart rate variability (HRV)** measures beat-to-beat variability
    in the heart rhythm. The heart is controlled by both the sympathic
    and the parasympathic nervesystem. The HRV can be used as a proxy
    for the balance between the two nerve systems (reference)

### Statistical Analysis

We wanted to find out whether there are potential clusters or groupings
that may distinguish or characterize neuropathy based on using multiple
neuropathy assessments. The most appropriate techniques for this
objective are cluster- and factor-type analyses. For clustering we used
hierarchical cluster analysis (HCA), which is a common and established
technique for finding clusters *between individual participants* and
their responses to the assessments. Given that the data values are
categorical rather than numeric, we used the Gower (Gower 1971)
calculation for computing the distance metric needed for the HCA. For
the HCA we used the complete linkage agglomeration method as it tends to
find compact clusters and more balanced cluster groups. (Note: The other
linkage methods produced highly unbalanced groups, meaning very few
individuals were assigned to them).

Factor analysis is an established technique for finding groupings
*between variables*, that is, between the individual neuropathy
assessment items (the variables). A limitation of standard factor
analysis is how it deals with discrete data, such as the responses to
the assessments (e.g. "absent", "present but minimal", "present" for
some assessments). To overcome this limitation, we used the Factor
Analysis of Mixed Data (FAMD) (Pagès 2004) approach to include both the
continuous and discrete data.

While both HCA and FAMD are powerful techniques, they have limitations
to addressing the objective. HCA strictly classifies individuals into a
prespecified number of clusters $k$ and FAMD finds the number of
"factors" based on how the variance in the data is maximized. Both
approaches can be sensitive to the conditions of the given dataset and
may not apply to other study samples.

TODO: Clarify this sentence.

For this analysis, we wanted to identify: the *probability* that any
given participant be assigned to a cluster based on their responses
within any given random sample of the data; and, the *likelihood* of any
given neuropathy assessment item strongly contributing to the underlying
variance within any given random sample of the data. This approach
required generated many random sub-samples of the neuropathy assessment
dataset and running the statistical techniques on each sub-sample. The
approach has these sequence of steps:

1.  Split data into two sets: a training set (85% of total sample) and a
    testing set (15% of total sample) to later cross-validate (CV) the
    findings.

2.  Split the training set into 100 randomly resampled sets of data by
    running 4-fold CV (4 random resampled sets of the data). This
    process was repeated 25 times. This enables us to obtain a
    distribution of results from the analyses.

3.  HCA and FAMD analyses were applied separately to each individual
    resampled set.

4.  For each HCA computation we extracted the individuals' cluster
    number and for each FAMD computation we extracted the assessment
    items' contributions to the components (i.e. the factors) and their
    explained variance. Given the large number of variables and results
    that would be extracted from the FAMD analysis, we kept only the
    assessments that contributed in the top 10 for each resampled set.

5.  After applying the analyses to each resampled set, the results were
    combined and we then kept the most commonly given responses to the
    assessments (for the HCA) or the assessment items that most
    contributed to the underlying variance (for FAMD).

    -   For HCA, "most common" was defined as the top 25% of
        > consistently given responses to the assessments for the given
        > cluster.

    -   For FAMD, "most contributed" was defined as those assessments in
        > the top 10 of contributions (as defined above for each
        > resampled set). If the final top contributors did not
        > contribute at least 10 times of all resampling done, they were
        > dropped.

TODO: Clarify FAMD section.

We found a high number of missingness in the DPN {{confirm name}} and
HRV assessments, so we ran the analyses on A) the full dataset and B)
data without the DPN and HRV assessments. The exact implementation of
this algorithm can be found in the code, available in the [project
repository]. We used for the statistical computing. The R packages
cluster (2.1.0) (Maechler et al. 2019) for the HCA and Gower distance
analysis and FactoMineR (1.42) (Lê, Josse, and Husson 2008) for the FAMD
analysis.

TODO: Include ref for the code

NOTE: MeanPercent for HCA is, for a given assessment item, the average
of the percent of times that a participant was assigned the cluster
number, for each randomly selected subset of the dataset. So, a higher
MeanPercent indicates that the assessment item was most commonly chosen
by those who were most likely to be assigned a given cluster number.
E.g. if "Numbness in the foot - Yes" had a mean percent of 90% in
cluster 1, that indicates the assessment response was mostly chosen by
those who were highly likely to be in cluster 1.

Conclusion
----------

TODO: Update this

Individuals can be classified into clusters by their specific responses
to the neuropathy assessments and only a few assessment items may be
needed to identify neuropathy cases. Using data-driven clustering and
feature extraction algorithms may help arrive at a consensus on
assessing neuropathy.

References
----------

Gower, J. C. 1971. "A General Coefficient of Similarity and Some of Its
Properties." *Biometrics* 27 (4). JSTOR: 857.
<https://doi.org/10.2307/2528823>.

Lê, Sébastien, Julie Josse, and François Husson. 2008. "FactoMineR: A
Package for Multivariate Analysis." *Journal of Statistical Software* 25
(1): 1--18. <https://doi.org/10.18637/jss.v025.i01>.

Maechler, Martin, Peter Rousseeuw, Anja Struyf, Mia Hubert, and Kurt
Hornik. 2019. *Cluster: Cluster Analysis Basics and Extensions*.

Pagès, J. 2004. "Analyse Factorielle de Données Mixtes." *Revue de
Statistique Appliquée* 52 (4). Société française de statistique:
93--111. <http://www.numdam.org/item/RSA_2004__52_4_93_0>.

[^1]: Items are defined as a specific question or assessment measurement
    used in the neuropathy assessment tools.

[^2]: Tools are defined as any established grouping of questions or
    measurements used clinically when diagnosing neuropathy (e.g. TCSS).

