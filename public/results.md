# Results {#results}


![](https://img.shields.io/badge/document%20status-rough%20draft-red?style=flat-square)




## Characteristics of the assessments



<img src="results_files/figure-html/unnamed-chunk-2-1.png" width="672" />

Table \@ref(tab:tab-response-counts) lists all the neuropathy assessments
and their responses with the number of times the response was given.
Table \@ref(tab:tab-cases-from-assessments) lists the different neuropathy assessments 
collected in ADDITION and the number of neuropathy cases classified by each.
As is shown, there are differences between each in how many are classified as
"having neuropathy".

- TODO: Include missingness information in this table?
- TODO: Confirm the threshold values used here
- TODO: Define threshold or criteria used for UENS, mTCSS
- TODO: Define criteria for HRV, etc... or at least how to present it
- TODO: Calculate "correlation" between scores

<table>
<caption>(\#tab:tab-cases-from-assessments)'Diagnoses' of neuropathy based on the various assessments.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> Assessment </th>
   <th style="text-align:right;"> NeuropathyCases </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> MNSI Questionnaire </td>
   <td style="text-align:right;"> 58 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Exam </td>
   <td style="text-align:right;"> 159 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Questionnaire </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Exam </td>
   <td style="text-align:right;"> 171 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Score </td>
   <td style="text-align:right;"> 186 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Questionnaire </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Score </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DPN Check </td>
   <td style="text-align:right;"> 146 </td>
  </tr>
</tbody>
</table>

## Hierachical cluster analysis

Tables \@ref(tab:tab-hca-common-responses-ranked-all) 
and \@ref(tab:tab-hca-common-responses-ranked-some)
below show the results of the most common responses given within
each cluster. The tables are divided into which questionnaire the
responses come from, what the value of the response is, what the
assessment item's description is, and a combination of the description
with response. The number given in the tables is the number of times the
given item (questionnaire, response, description, etc) occurs in the
cluster. There are two sets of tables: analyses using all the assessment
items and those excluding the DPN and HRV assessments.

Some general observations: Cluster 1 definitely seems to be those who
would not have neuropathy; Cluster 2 and 3 seem to differ in the
monofilament responses, where cluster 3 has negative responses to this
assessment.

The appendix figure below shows how likely participants are to be
assigned a cluster of 1, 2, or 3 (when the cluster k is fixed as either
2 or 3). Based on the figure, most people tend to be assigned to cluster
1. When k is fixed to 3, more participants are likely to be assigned to
cluster 2.



<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-hca-common-responses-ranked-some)Most common assessment items and their responses in each cluster, ranked by the contribution to that cluster. *For the **HCA analysis** using assessment items **excluding** the DPN Check and HRV assessments*.</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> Rank </th>
   <th style="text-align:left;"> Cluster: 1 </th>
   <th style="text-align:left;"> Cluster: 2 </th>
   <th style="text-align:left;"> Cluster: 3 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> Pain caused or increased by brushing?: Yes </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> Have you ever had an amputation?: Yes </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: No </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> Light touch(monofilament): Normal </td>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> Foot pain?: No </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> Vibration sensation: Normal </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> Vibration sensation for foot: Normal(&lt;10sec) </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> Pin prick: Normal </td>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> Vibration sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> Monofilament great toe: Normal(8-10) </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> Do you ever have any prickling feelings in your legs&amp;feet?: No </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Ulceration foot?: Present </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> Light touch under foot, point 1: Normal(≥ 2/3) </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Pin prick: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Foot pain?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> Temperature for foot: Normal </td>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> Vibration perception at great toe: Present </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> Are your legs and/or feet numb?: No </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> Light touch (monofilament) for foot: Normal(≥5/8) </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> Light touch under foot, point 2: Normal(≥ 2/3) </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
  </tr>
</tbody>
</table>

## Factor Analysis of Mixed Data

The FAMD seems to show some differences (when aggregated all together)
between components. For instance, component 1 is mostly about foot pain
(i.e. DN4 assessment). Component 2 seems to reflect mainly ankle reflex,
foot pain, and reduced sensation moving up the toes and foot. Component
3 is almost entirely dominated with ankle and knee reflex. Component 4
is a mix of ankle reflex tingling sensations, and ataxia, as well as
starting to have upper limb symptoms. Finally, Component 5 is around
temperature, position, and vibration sensation, general numbness and
weakness.

Based on these supplementary figures, each individual assessment item
doesn't contribute much to any given component, most of them being
around 2 to 5%. Nor does the FAMD pick up much overall explained
variance in the data, based on 5 components. First component has a range
of explained variance between 9 to 16% and the second component ranges
from 6 to 8%.




<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-famd-common-responses-ranked-some)Most common assessment items and their responses in each component, ranked by the contribution to that component. *For the **FAMD analysis** using assessment items **excluding** the DPN Check and HRV assessments*.</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> Rank </th>
   <th style="text-align:left;"> Component: 1 </th>
   <th style="text-align:left;"> Component: 2 </th>
   <th style="text-align:left;"> Component: 3 </th>
   <th style="text-align:left;"> Component: 4 </th>
   <th style="text-align:left;"> Component: 5 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;"> Position sensation: Decreased on toes </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Pain caused or increased by brushing?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:left;"> Pain caused or increased by brushing?: Yes </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Ulceration foot?: Present </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Position sensation: Decreased on toes </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
   <td style="text-align:left;"> Ataxia?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:left;"> Neurotip section 5: Absent </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Knee reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Knee reflex: Absent </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Ataxia?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Neurotip section 2: Decreased </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
  </tr>
</tbody>
</table>

## Comparing HCA and FAMD

There are ~250 combined items and responses.

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:table-comparing-shared-items-some)Shared top assessment items between the FAMD and HCA results, for analyses with DPN Check and HRV **excluded**.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> HCA </th>
   <th style="text-align:left;"> FAMD </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are your legs and/or feet numb?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Ataxia?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you ever have any prickling feelings in your legs&amp;feet?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an amputation?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Knee reflex: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Knee reflex: Decreased(present by reinforcement) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch (monofilament) for foot: Normal(≥5/8) </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 1: Normal(≥ 2/3) </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 2: Normal(≥ 2/3) </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch(monofilament): Normal </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe: Normal(8-10) </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Neurotip section 2: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Neurotip section 5: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being, not daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain caused or increased by brushing?: Yes </td>
   <td style="text-align:left;"> Pain caused or increased by brushing?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick: Normal </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Position sensation: Decreased on toes </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Temperature for foot: Normal </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ulceration foot?: Present </td>
   <td style="text-align:left;"> Ulceration foot?: Present </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being, not daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration perception at great toe: Present </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation for foot: Normal(&lt;10sec) </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation: Normal </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being, not daily living </td>
  </tr>
</tbody>
</table>




## Top FAMD assessments in HCA analysis




## Score based on analyses

<img src="results_files/figure-html/unnamed-chunk-5-1.png" width="672" /><img src="results_files/figure-html/unnamed-chunk-5-2.png" width="672" /><img src="results_files/figure-html/unnamed-chunk-5-3.png" width="672" />

## Supplemental material

### Basic descriptive tables

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-response-counts)Count of each response to an assessment item for participants in the ADDITION cohort.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> Assessment </th>
   <th style="text-align:left;"> Response </th>
   <th style="text-align:right;"> Count </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> DN4 Q1: Do you suffer from pain in your feet? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 510 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 70 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q10: Pain in area may reveal hypoesthesia to prick? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 257 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 299 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q11: Pain caused or increased by brushing? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 281 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 298 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q2: Pain feels like burning? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 46 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 510 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q3: Pain feels like painful cold? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 61 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 511 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q4: Pain feels like electric shocks? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 51 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 511 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q5: Is pain associated with tingling? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 510 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q6: Is pain associated with pins&amp;needles? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 511 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q7: Is pain associated with numbness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 52 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 511 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 17 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q8: Is pain associated with itching? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 59 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 511 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q9: Pain in area may reveal hypoesthesia to touch? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 264 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 298 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q1: Ulceration foot? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 575 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q2: Ankle reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 48 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 239 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present </td>
   <td style="text-align:right;"> 305 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present with reinforcement </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q3: Vibration perception at great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 169 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 55 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present </td>
   <td style="text-align:right;"> 352 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q4: Monofilament great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent(0) </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased(1-7) </td>
   <td style="text-align:right;"> 97 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(8-10) </td>
   <td style="text-align:right;"> 457 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q1: Ulceration foot? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 576 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q2: Ankle reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 246 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present </td>
   <td style="text-align:right;"> 300 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present with reinforcement </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q3: Vibration perception at great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 193 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 64 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> present </td>
   <td style="text-align:right;"> 320 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q4: Monofilament great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent(0) </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased(1-7) </td>
   <td style="text-align:right;"> 97 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(8-10) </td>
   <td style="text-align:right;"> 459 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q1: Are your legs and/or feet numb? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 495 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 88 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q10: Do you feel weak all over most of time? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 505 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 77 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q11: Are your symptoms worse at night? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 507 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 74 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q12: Do your legs hurt when you walk? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 473 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q13: Are you able to sense your feet when you walk? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 31 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 550 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q14: Is skin on your feet so dry that it cracks open? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 499 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 84 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q15: Have you ever had an amputation? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 571 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q2: Do you ever have any burning pain in your legs and/or feet? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 512 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 71 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q3: Are your feet too sensitive to touch? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 535 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 48 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q4: Do you get muscle cramps in your legs and/or feet? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 371 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 211 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q5: Do you ever have any prickling feelings in your legs&amp;feet? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 438 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 144 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q6: Does it hurt when bed covers touch your skin? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 556 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q7: In tub or shower, can tell hot from cold water? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 19 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 564 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q8: Have you ever had an open sore on your foot? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 561 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q9: Has your doctor ever told you that you have diabetic neuropathy? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 552 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q1: Light touch under foot, point 1 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 86 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 492 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q2: Light touch under foot, point 2 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 76 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 503 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q3: Light touch under foot, point 3 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 88 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 491 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q4: Light touch under foot, point 4 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 86 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 493 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q1: Light touch under foot, point 1 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 78 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 500 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q2: Light touch under foot, point 2 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 80 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 498 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q3: Light touch under foot, point 3 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 85 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 493 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q4: Light touch under foot, point 4 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤1/3) </td>
   <td style="text-align:right;"> 80 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥ 2/3) </td>
   <td style="text-align:right;"> 498 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q1: Foot pain? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 525 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q10: Ataxia? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 553 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:right;"> 17 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being, not daily living </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q11: Upper limb symptoms? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 507 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 72 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q12: Upper limb symptoms? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 522 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being, not daily living </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q13: Pin prick </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at ankle or absent on toes </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at toes but only until ankle </td>
   <td style="text-align:right;"> 44 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased on toes </td>
   <td style="text-align:right;"> 45 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 446 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q14: Temperature </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 45 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at ankle or absent on toes </td>
   <td style="text-align:right;"> 120 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at toes but only until ankle </td>
   <td style="text-align:right;"> 63 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased on toes </td>
   <td style="text-align:right;"> 114 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 276 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q15: Light touch(monofilament) </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at ankle or absent on toes </td>
   <td style="text-align:right;"> 67 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at toes but only until ankle </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased on toes </td>
   <td style="text-align:right;"> 90 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 376 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q16: Vibration sensation </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at ankle or absent on toes </td>
   <td style="text-align:right;"> 212 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at toes but only until ankle </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased on toes </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 270 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q17: Position sensation </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at ankle or absent on toes </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased at toes but only until ankle </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased on toes </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 556 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q2: Foot pain? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 533 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being, not daily living </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q3: Numbness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 45 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 495 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 78 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q4: Numbness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 509 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being, not daily living </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q5: Tingling? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 466 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 110 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q6: Tingling? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 478 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:right;"> 66 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being, not daily living </td>
   <td style="text-align:right;"> 15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q7: Weakness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 529 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 47 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q8: Weakness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 539 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:right;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Yes, interferes in well-being, not daily living </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q9: Ataxia? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 547 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> pass </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q1: Pin prick (neurotip) for foot </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤4/8) </td>
   <td style="text-align:right;"> 48 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥5/8) </td>
   <td style="text-align:right;"> 530 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q1Q: Foot pain? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 525 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q2: Temperature for foot </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal </td>
   <td style="text-align:right;"> 292 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 287 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q2Q: Numbness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 45 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 495 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 78 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q3: Light touch (monofilament) for foot </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≤4/8) </td>
   <td style="text-align:right;"> 77 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(≥5/8) </td>
   <td style="text-align:right;"> 501 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q3Q: Tingling? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 466 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 110 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q4: Vibration sensation for foot </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal(≥10sec) </td>
   <td style="text-align:right;"> 301 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal(&lt;10sec) </td>
   <td style="text-align:right;"> 277 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q4Q: Weakness? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 529 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 47 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q5: Position sensation for great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> abnormal </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 555 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q5Q: Ataxia? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 547 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q6: Knee reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 50 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 95 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased(present by reinforcement) </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 441 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q6Q: Upper limb symptoms? </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> no </td>
   <td style="text-align:right;"> 507 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> yes </td>
   <td style="text-align:right;"> 72 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q7: Knee reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 98 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased(present by reinforcement) </td>
   <td style="text-align:right;"> 30 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 436 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q8: Ankle reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 44 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 249 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased(present by reinforcement) </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 296 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q9: Ankle reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 248 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased(present by reinforcement) </td>
   <td style="text-align:right;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 300 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q1: Extension great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 59 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 531 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q10: Position sense great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 560 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q11: Ankle reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 44 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 240 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 308 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q2: Neurotip section 1 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 60 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 491 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q3: Neurotip section 2 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 42 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 515 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q4: Neurotip section 3 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 13 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 542 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q5: Neurotip section 4 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 551 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q6: Neurotip section 5 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 13 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 561 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q7: Neurotip section 6 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 569 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q8: Allodynia leg </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 49 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> In toes/foot </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 565 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q9: Vibration on great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 45 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 166 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q1: Extension great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 66 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q10: Position sense great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 566 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q11: Ankle reflex </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 44 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 246 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q2: Neurotip section 1 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 59 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 495 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q3: Neurotip section 2 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 46 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 513 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q4: Neurotip section 3 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 13 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 543 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q5: Neurotip section 4 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 17 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 558 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q6: Neurotip section 5 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 568 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q7: Neurotip section 6 </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 41 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 571 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q8: Allodynia leg </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 50 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> In toes/foot </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 566 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q9: Vibration on great toe </td>
   <td style="text-align:left;"> (Missing) </td>
   <td style="text-align:right;"> 45 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> absent </td>
   <td style="text-align:right;"> 195 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> decreased </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> normal </td>
   <td style="text-align:right;"> 316 </td>
  </tr>
</tbody>
</table>

### HCA analyses

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-hca-common-responses-ranked-all)Most common assessment items and their responses in each cluster, ranked by the contribution to that cluster. *For the **HCA analysis** using all assessment items, **including** the DPN Check and HRV assessments*.</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> Rank </th>
   <th style="text-align:left;"> Cluster: 1 </th>
   <th style="text-align:left;"> Cluster: 2 </th>
   <th style="text-align:left;"> Cluster: 3 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> Ankle reflex: Present </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> In tub or shower, can tell hot from cold water?: No </td>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> In tub or shower, can tell hot from cold water?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Extension great toe: Decreased </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> Amplitude: Mid-high </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Foot pain?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> Are you able to sense your feet when you walk?: No </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> Light touch under foot, point 2: Abnormal(≤1/3) </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:left;"> Foot pain?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> Knee reflex: Normal </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> Vibration sensation: Normal </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> Velocity: Mid-high </td>
   <td style="text-align:left;"> Foot pain?: Yes </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> Root mean square of successive differences for hr: Low-mid </td>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
   <td style="text-align:left;"> Are your feet too sensitive to touch?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Foot pain?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
</tbody>
</table>





<div class="figure">
<img src="results_files/figure-html/fig-hca-assessments-1.png" alt="Proponent that the assessment questionnaire was contained in the **HCA** cluster compared to all other assessments. A) is the results when all neuropathy assessment items are used, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results when **excluding** the DPN Check and HRV assessments and has a complete case sample size of 383." width="768" />
<p class="caption">(\#fig:fig-hca-assessments)Proponent that the assessment questionnaire was contained in the **HCA** cluster compared to all other assessments. A) is the results when all neuropathy assessment items are used, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results when **excluding** the DPN Check and HRV assessments and has a complete case sample size of 383.</p>
</div>

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-hca-assessments)Most common assessment items and their responses within each **HCA** cluster, for data from *all* assessments, *including* the DPN Check and HRV. If the item and reponse was present in the cluster, it is indicated by an *X*.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> AssessmentResponse </th>
   <th style="text-align:center;"> Cluster: 1 </th>
   <th style="text-align:center;"> Cluster: 2 </th>
   <th style="text-align:center;"> Cluster: 3 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Present </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Amplitude: Mid-high </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are you able to sense your feet when you walk?: No </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> In tub or shower, can tell hot from cold water?: No </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Knee reflex: Normal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 2: Abnormal(≤1/3) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Root mean square of successive differences for hr: Low-mid </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Velocity: Mid-high </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation: Normal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Extension great toe: Decreased </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are your feet too sensitive to touch?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
</tbody>
</table>

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-hca-assessments)Most common assessment items and their responses within each **HCA** cluster, for data from assessment tools *excluding* the DPN and HRV assessments. If the item and reponse was present in the cluster, it is indicated by an *X*.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> AssessmentResponse </th>
   <th style="text-align:center;"> Cluster: 1 </th>
   <th style="text-align:center;"> Cluster: 2 </th>
   <th style="text-align:center;"> Cluster: 3 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Foot pain?: No </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe: Normal(8-10) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are your legs and/or feet numb?: No </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you ever have any prickling feelings in your legs&amp;feet?: No </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: No </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an amputation?: Yes </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch (monofilament) for foot: Normal(≥5/8) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 1: Normal(≥ 2/3) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 2: Normal(≥ 2/3) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch(monofilament): Normal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain caused or increased by brushing?: Yes </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick: Normal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Temperature for foot: Normal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration perception at great toe: Present </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation for foot: Normal(&lt;10sec) </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation: Normal </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 2: Absent </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 3: Absent </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sense great toe: Decreased </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 4: Absent </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 4: Decreased </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick: Decreased at ankle or absent on toes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation: Decreased at toes but only until ankle </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ulceration foot?: Present </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
</tbody>
</table>

<img src="results_files/figure-html/fig-hca-cluster-likelihood-1.png" width="672" />

### FAMD Analyses



<div class="figure">
<img src="results_files/figure-html/fig-famd-assessments-1.png" alt="Proponent that the assessment questionnaire was contained in the **FAMD** component compared to all other assessments. A) is the results when all neuropathy assessment items are used, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results when **excluding** the DPN Check and HRV assessments and has a complete case sample size of 383." width="864" />
<p class="caption">(\#fig:fig-famd-assessments)Proponent that the assessment questionnaire was contained in the **FAMD** component compared to all other assessments. A) is the results when all neuropathy assessment items are used, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results when **excluding** the DPN Check and HRV assessments and has a complete case sample size of 383.</p>
</div>

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-famd-assessments)Most common assessment items that make up each **FAMD** component, for data from *all* assessment tools, **including** the DPN Check and HRV assessments.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> NeuropathyDescription </th>
   <th style="text-align:center;"> Component: 1 </th>
   <th style="text-align:center;"> Component: 2 </th>
   <th style="text-align:center;"> Component: 3 </th>
   <th style="text-align:center;"> Component: 4 </th>
   <th style="text-align:center;"> Component: 5 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Foot pain? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Weakness? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 1 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch (monofilament) for foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 2 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch(monofilament) </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 4 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick (neurotip) for foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration perception at great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration on great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Knee reflex </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Numbness? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Tingling? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sense great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Upper limb symptoms? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation for great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation for foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an open sore on your foot? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are your legs and/or feet numb? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Temperature for foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Temperature </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Extension great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
</tbody>
</table>

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-famd-assessments)Most common assessment items that make up each **FAMD** component, for data from assessment tools **excluding** the DPN Check and HRV assessments.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> NeuropathyDescription </th>
   <th style="text-align:center;"> Component: 1 </th>
   <th style="text-align:center;"> Component: 2 </th>
   <th style="text-align:center;"> Component: 3 </th>
   <th style="text-align:center;"> Component: 4 </th>
   <th style="text-align:center;"> Component: 5 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Monofilament great toe </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 1 </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 2 </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick (neurotip) for foot </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch (monofilament) for foot </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Numbness? </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 3 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 4 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 5 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sense great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Allodynia leg </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 6 </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Knee reflex </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration perception at great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration on great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation for great toe </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain caused or increased by brushing? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Tingling? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Weakness? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation for foot </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
   <td style="text-align:center;"> X </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Upper limb symptoms? </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;">  </td>
   <td style="text-align:center;"> X </td>
  </tr>
</tbody>
</table>

<div class="figure">
<img src="results_files/figure-html/fig-famd-contribs-1.png" alt="Top neuropathy assessment items' contributions (%) to each **FAMD** component across all resampled sets. A) is the results when all neuropathy assessment items are used, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results **excluding** the DPN Check and HRV assessment items and has a complete case sample size of 383." width="816" />
<p class="caption">(\#fig:fig-famd-contribs)Top neuropathy assessment items' contributions (%) to each **FAMD** component across all resampled sets. A) is the results when all neuropathy assessment items are used, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results **excluding** the DPN Check and HRV assessment items and has a complete case sample size of 383.</p>
</div>

<div class="figure">
<img src="results_files/figure-html/fig-famd-expl-var-1.png" alt="Explained variance of each **FAMD** component across all resampled sets. A) is the results when all neuropathy assessment items, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results **excluding** the DPN Check and HRV assessment items and has a complete case sample size of 383." width="672" />
<p class="caption">(\#fig:fig-famd-expl-var)Explained variance of each **FAMD** component across all resampled sets. A) is the results when all neuropathy assessment items, **including** the DPN Check and HRV assessments, and has a complete case sample size of 183. B) is the results **excluding** the DPN Check and HRV assessment items and has a complete case sample size of 383.</p>
</div>

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab-famd-common-responses-ranked-all)Most common assessment items and their responses in each component, ranked by the contribution to that component. *For the **FAMD analysis** using all assessment items, **including** the DPN Check and HRV assessments*.</caption>
 <thead>
  <tr>
   <th style="text-align:right;"> Rank </th>
   <th style="text-align:left;"> Component: 1 </th>
   <th style="text-align:left;"> Component: 2 </th>
   <th style="text-align:left;"> Component: 3 </th>
   <th style="text-align:left;"> Component: 4 </th>
   <th style="text-align:left;"> Component: 5 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> In tub or shower, can tell hot from cold water?: No </td>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> Foot pain?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being, not daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> Foot pain?: Yes </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Vibration on great toe: Decreased </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Numbness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Vibration perception at great toe: Decreased </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:left;"> Light touch (monofilament) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:left;"> Have you ever had an amputation?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Numbness?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:left;"> Ataxia?: Yes </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Tingling?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Tingling?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Light touch (monofilament) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;"> Light touch under foot, point 1: Abnormal(≤1/3) </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy?: Yes </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:left;"> Knee reflex: Absent </td>
   <td style="text-align:left;"> Numbness?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:left;"> Neurotip section 2: Decreased </td>
   <td style="text-align:left;"> Light touch under foot, point 2: Abnormal(≤1/3) </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Weakness?: Yes </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;">  </td>
  </tr>
</tbody>
</table>

### Comparing HCA and FAMD ranked items

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:table-comparing-shared-items-all)Shared top assessment items between the FAMD and HCA results, for analyses with *all* assessment items **included**.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> HCA </th>
   <th style="text-align:left;"> FAMD </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
   <td style="text-align:left;"> Allodynia leg: In toes/foot </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Amplitude: Mid-high </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
   <td style="text-align:left;"> Ankle reflex: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
   <td style="text-align:left;"> Ankle reflex: Decreased(present by reinforcement) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Present </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
   <td style="text-align:left;"> Ankle reflex: Present with reinforcement </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are you able to sense your feet when you walk?: No </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Are your feet too sensitive to touch?: Yes </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Ataxia?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
   <td style="text-align:left;"> Ataxia?: Yes, interferes in well-being, not daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
   <td style="text-align:left;"> Does it hurt when bed covers touch your skin?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Extension great toe: Decreased </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes </td>
   <td style="text-align:left;"> Foot pain?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Foot pain?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Foot pain?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy?: Yes </td>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Have you ever had an amputation?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
   <td style="text-align:left;"> Have you ever had an open sore on your foot?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> In tub or shower, can tell hot from cold water?: No </td>
   <td style="text-align:left;"> In tub or shower, can tell hot from cold water?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
   <td style="text-align:left;"> Is pain associated with itching?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
   <td style="text-align:left;"> Is pain associated with itching?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
   <td style="text-align:left;"> Is pain associated with numbness?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
   <td style="text-align:left;"> Is pain associated with numbness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
   <td style="text-align:left;"> Is pain associated with pins&amp;needles?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
   <td style="text-align:left;"> Is pain associated with tingling?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
   <td style="text-align:left;"> Is pain associated with tingling?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Knee reflex: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Knee reflex: Normal </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Light touch (monofilament) for foot: Abnormal(≤4/8) </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Light touch under foot, point 1: Abnormal(≤1/3) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Light touch under foot, point 2: Abnormal(≤1/3) </td>
   <td style="text-align:left;"> Light touch under foot, point 2: Abnormal(≤1/3) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
   <td style="text-align:left;"> Monofilament great toe: Absent(0) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
   <td style="text-align:left;"> Neurotip section 1: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Neurotip section 2: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
   <td style="text-align:left;"> Neurotip section 5: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
   <td style="text-align:left;"> Neurotip section 6: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Numbness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Numbness?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Numbness?: Yes, interferes in well-being, not daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
   <td style="text-align:left;"> Pain feels like burning?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
   <td style="text-align:left;"> Pain feels like burning?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
   <td style="text-align:left;"> Pain feels like electric shocks?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
   <td style="text-align:left;"> Pain feels like painful cold?: No </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
   <td style="text-align:left;"> Pain feels like painful cold?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to prick?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
   <td style="text-align:left;"> Pain in area may reveal hypoesthesia to touch?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
   <td style="text-align:left;"> Pin prick (neurotip) for foot: Abnormal(≤4/8) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
   <td style="text-align:left;"> Position sensation for great toe: Abnormal </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
   <td style="text-align:left;"> Position sensation: Decreased at ankle or absent on toes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
   <td style="text-align:left;"> Position sense great toe: Absent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Root mean square of successive differences for hr: Low-mid </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Tingling?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Upper limb symptoms?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Velocity: Mid-high </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Vibration on great toe: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Vibration perception at great toe: Decreased </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vibration sensation: Normal </td>
   <td style="text-align:left;">  </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Weakness?: Yes </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
   <td style="text-align:left;"> Weakness?: Yes, but not interferes in well-being/daily living </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being &amp; daily living </td>
   <td style="text-align:left;"> Weakness?: Yes, interferes in well-being &amp; daily living </td>
  </tr>
</tbody>
</table>

### Table of questionnaires and assessment items

<table class="table table-condensed table-striped" style="margin-left: auto; margin-right: auto;">
<caption>(\#tab:unnamed-chunk-6)All 105 assessment items and the item's description.</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> Assessment Item </th>
   <th style="text-align:left;"> Description </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> HRV RESTSTANDRATIO </td>
   <td style="text-align:left;"> RS (rest to standing) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> HRV EXPIREINSPIRERATIO </td>
   <td style="text-align:left;"> E:I (expiration inspiration) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> HRV RMSSD </td>
   <td style="text-align:left;"> Root mean square of the successive differences for HR </td>
  </tr>
  <tr>
   <td style="text-align:left;"> HRV HIGHFREQ </td>
   <td style="text-align:left;"> High Frequency (?) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> HRV SDNN </td>
   <td style="text-align:left;"> Standard deviation of the NN (R-R) intervals </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q1 </td>
   <td style="text-align:left;"> Are your legs and/or feet numb? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q2 </td>
   <td style="text-align:left;"> Do you ever have any burning pain in your legs and/or feet? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q3 </td>
   <td style="text-align:left;"> Are your feet too sensitive to touch? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q4 </td>
   <td style="text-align:left;"> Do you get muscle cramps in your legs and/or feet? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q5 </td>
   <td style="text-align:left;"> Do you ever have any prickling feelings in your legs and feet? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q6 </td>
   <td style="text-align:left;"> Does it hurt when the bed covers touch your skin? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q7 </td>
   <td style="text-align:left;"> When you get into the tub or shower, are you able to tell the hot water from the cold water? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q8 </td>
   <td style="text-align:left;"> Have you ever had an open sore on your foot? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q9 </td>
   <td style="text-align:left;"> Has your doctor ever told you that you have diabetic neuropathy? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q10 </td>
   <td style="text-align:left;"> Do you feel weak all over most of the time? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q11 </td>
   <td style="text-align:left;"> Are your symptoms worse at night? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q12 </td>
   <td style="text-align:left;"> Do your legs hurt when you walk? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q13 </td>
   <td style="text-align:left;"> Are you able to sense your feet when you walk? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q14 </td>
   <td style="text-align:left;"> Is the skin on your feet so dry that it cracks open? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI Q15 </td>
   <td style="text-align:left;"> Have you ever had an amputation? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q1 </td>
   <td style="text-align:left;"> Ulceration right foot? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q1 </td>
   <td style="text-align:left;"> Ulceration left foot? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q2 </td>
   <td style="text-align:left;"> Ankle Reflexes Right side </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q2 </td>
   <td style="text-align:left;"> Ankle Reflexes Left side </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q3 </td>
   <td style="text-align:left;"> Vibration perception at great toe - right side </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q3 </td>
   <td style="text-align:left;"> Vibration perception at great toe - left side </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (R) Q4 </td>
   <td style="text-align:left;"> Monofilament right great toe </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MNSI (L) Q4 </td>
   <td style="text-align:left;"> Monofilament left great toe </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q1 </td>
   <td style="text-align:left;"> Do you suffer from pain in your feet? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q2 </td>
   <td style="text-align:left;"> Does the pain have the following characteristics Burning? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q3 </td>
   <td style="text-align:left;"> Does the pain have the following characteristics Painful cold? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q4 </td>
   <td style="text-align:left;"> Does the pain have the following characteristics Electric shocks? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q5 </td>
   <td style="text-align:left;"> Is the pain associated with Tingling? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q6 </td>
   <td style="text-align:left;"> Is the pain associated with Pins and Needles? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q7 </td>
   <td style="text-align:left;"> Is the pain associated with Numbness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q8 </td>
   <td style="text-align:left;"> Is the pain associated with Itching? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q9 </td>
   <td style="text-align:left;"> Is the pain located in an area where the physical examination may reveal Hypoesthesia to touch? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q10 </td>
   <td style="text-align:left;"> Is the pain located in an area where the physical examination may reveal Hypoesthesia to prick? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DN4 Q11 </td>
   <td style="text-align:left;"> In the painful area, can the pain be caused or increased by Brushing ? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q1 </td>
   <td style="text-align:left;"> Foot pain(neuropathic)? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q2 </td>
   <td style="text-align:left;"> Foot pain(neuropathic)? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q3 </td>
   <td style="text-align:left;"> Numbness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q4 </td>
   <td style="text-align:left;"> Numbness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q5 </td>
   <td style="text-align:left;"> Tingling? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q6 </td>
   <td style="text-align:left;"> Tingling? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q7 </td>
   <td style="text-align:left;"> Weakness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q8 </td>
   <td style="text-align:left;"> Weakness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q9 </td>
   <td style="text-align:left;"> Ataxia? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q10 </td>
   <td style="text-align:left;"> Ataxia? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q11 </td>
   <td style="text-align:left;"> Upper limb symptoms? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q12 </td>
   <td style="text-align:left;"> Upper limb symptoms? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q13 </td>
   <td style="text-align:left;"> Pin prick sensation </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q14 </td>
   <td style="text-align:left;"> Temperature sensation </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q15 </td>
   <td style="text-align:left;"> Light touch sensation(monofilament) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q16 </td>
   <td style="text-align:left;"> Vibration sensation </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mTCSS Q17 </td>
   <td style="text-align:left;"> Position sensation </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q1 </td>
   <td style="text-align:left;"> Light touch under the right foot, point 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q2 </td>
   <td style="text-align:left;"> Light touch under the right foot, point 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q3 </td>
   <td style="text-align:left;"> Light touch under the right foot, point 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (R) Q4 </td>
   <td style="text-align:left;"> Light touch under the right foot, point 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q1 </td>
   <td style="text-align:left;"> Light touch under the left foot, point 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q2 </td>
   <td style="text-align:left;"> Light touch under the left foot, point 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q3 </td>
   <td style="text-align:left;"> Light touch under the left foot, point 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Monofilament (L) Q4 </td>
   <td style="text-align:left;"> Light touch under the left foot, point 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q1 </td>
   <td style="text-align:left;"> extension great toe right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q2 </td>
   <td style="text-align:left;"> Neurotip section 1 right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q3 </td>
   <td style="text-align:left;"> Neurotip section 2 right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q4 </td>
   <td style="text-align:left;"> Neurotip section 3 right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q5 </td>
   <td style="text-align:left;"> Neurotip section 4 right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q6 </td>
   <td style="text-align:left;"> Neurotip section 5 right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q7 </td>
   <td style="text-align:left;"> Neurotip section 6 right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q8 </td>
   <td style="text-align:left;"> Allodynia right leg </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q9 </td>
   <td style="text-align:left;"> Vibration on great toe right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q10 </td>
   <td style="text-align:left;"> Position sense right great toe </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (R) Q11 </td>
   <td style="text-align:left;"> ankle reflex right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q1 </td>
   <td style="text-align:left;"> extension great toe left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q2 </td>
   <td style="text-align:left;"> Neurotip section 1 left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q3 </td>
   <td style="text-align:left;"> Neurotip section 2 left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q4 </td>
   <td style="text-align:left;"> Neurotip section 3 left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q5 </td>
   <td style="text-align:left;"> Neurotip section 4 left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q6 </td>
   <td style="text-align:left;"> Neurotip section 5 left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q7 </td>
   <td style="text-align:left;"> Neurotip section 6 left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q8 </td>
   <td style="text-align:left;"> Allodynia left leg </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q9 </td>
   <td style="text-align:left;"> Vibration on great toe left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q10 </td>
   <td style="text-align:left;"> Position sense left great toe </td>
  </tr>
  <tr>
   <td style="text-align:left;"> UENS (L) Q11 </td>
   <td style="text-align:left;"> ankle reflex left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q1Q </td>
   <td style="text-align:left;"> Foot pain(neuropathic)? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q2Q </td>
   <td style="text-align:left;"> Numbness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q3Q </td>
   <td style="text-align:left;"> Tingling? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q4Q </td>
   <td style="text-align:left;"> Weakness? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q5Q </td>
   <td style="text-align:left;"> Ataxia? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q6Q </td>
   <td style="text-align:left;"> Upper limb symptoms? </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q1 </td>
   <td style="text-align:left;"> Pin prick sensation (Neurotip) for the most abnormal foot </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q3 </td>
   <td style="text-align:left;"> Light touch sensation (monofilament) for the most abnormal foot </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q2 </td>
   <td style="text-align:left;"> Temperature sensation for the most abnormal foot </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q4 </td>
   <td style="text-align:left;"> Vibration sensation for the most abnormal foot </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q5 </td>
   <td style="text-align:left;"> Position sensation for the most abnormal great toe </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q6 </td>
   <td style="text-align:left;"> Knee reflex, dexter </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q7 </td>
   <td style="text-align:left;"> Knee reflex, sinistra </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q8 </td>
   <td style="text-align:left;"> Ankle reflex, dexter </td>
  </tr>
  <tr>
   <td style="text-align:left;"> TCSS Q9 </td>
   <td style="text-align:left;"> Ankle reflex, sinistra </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DPN (R) AMP </td>
   <td style="text-align:left;"> amplitude right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DPN (R) LED </td>
   <td style="text-align:left;"> velocity right </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DPN (L) AMP </td>
   <td style="text-align:left;"> amplitude left </td>
  </tr>
  <tr>
   <td style="text-align:left;"> DPN (L) LED </td>
   <td style="text-align:left;"> velocity left </td>
  </tr>
</tbody>
</table>

### Grading score for assessment items 

#### For diabetic polyneuropathy (DPN)

- **MNSI questionnaire**: Score >3
    - But, two questions are not included in the score; 
        - "Do you get muscle cramps in your leg/feet?" (i.e. number 4)
        - "Do you feel weak all over most of the time?" (i.e. number 10)
    - Important: 
        - "Yes" counts for a positive answer in questions 1-3, 5-6, 8-9, 11-12, and 14-15
        - "No" counts for a positive answer in questions 7 and 13

- **MNSI examination**: Score >2.5
    - In total for both feet, with a maximum score of 8

- **TCSS**: Score > 5
    - Ankle and knee reflexes scored on each side, 
    other examinations and symptoms on the two legs/feet together

- **mTCSS**: No validated cutoff

- **DN4** (painful neuropathy): Score >3

- **DPN Check** (assessment of sural nerve conduction): Amplitude =< 4 µV or conduction velocity =< 40 m/s
    - Because DPN is a symmetrical disease, 
    a diagnosis of DPN requires abnormal recordings by the DPN Check to be bilateral.
    The exception to this is if only 1 leg was accessible for recording,
    such as in cases of amputation or bandaging of the other leg,
    then we considered it as valid for a DPN diagnosis.

#### For cardiovascular autonomic neuropathy (CAN)

- **Heart rate variability (HRV) indices**: Continuous measures with no cutoff
    - Lower values indicate a more adverse profile
    - The indices RMSSD and HF are primarily considered measures of parasympathetic activity, 
    whereas SDNN, LF, total power, and LF/HF ratio are measures of both parasympathetic 
    and sympathetic activity. 
    Lower values of these CAN indices are considered to reflect lower HRV.

<!--
NOTE: We don't use CARTS in this analysis.
Cardiovascular reflex tests (CARTs) can be used for a diagnosis of CAN. At least
two (out of three) tests are required for a diagnosis.

RS – Lying to standing
EI – Deep breathing
VM – Valsalva maneuver
Two – or three abnormal tests required for the diagnosis. Using age- normative material

Table 3:  Normative data for evaluating CARTs by Cardone used in the ADDITION (64; 115).


-->

## References
