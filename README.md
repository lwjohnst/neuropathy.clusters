# Heterogeneity in neuropathy measures - A factor analysis from ADDITION-Denmark

To date, no accurate and concise definition on diabetic neuropathy exist. The
disease is considered a syndrome-like disease reflected by the heterogeneity of
clinical tests and tools used for diagnosing this disease. At the 13-year
clinical follow-up examination in ADDITION-Denmark a large number of different
measures of neuropathy were obtained. This exploratory data-driven study will
(1) identify clusters of participants sharing similar values of
measures/covariates of neuropathy and (2) provide opportunity to describe
patient characteristics of those clusters. We will (3) identify which measures
of neuropathy most strongly separates participants with neuropathy from
participants without neuropathy using “raw data” irrespective of the arbitrary
cut-offs used for a large number of these covariates. Potentially, these
analyses may provide evidence for a more accurate definition of neuropathy
requiring less amount of different assessments for diagnosing this disease.

**Objective**: We aim to identify potential clusters within multiple nerve
assessment tools that may characterize neuropathy in people with type 2 diabetes
and to describe which risk factors have the strongest association with these
groups.

## Current published material and presentations

### Poster presentation at EDEG, 2019

[![Website](https://img.shields.io/website/https/posters.lwjohnst.com/edeg-2019?up_message=online)](https://posters.lwjohnst.com/edeg-2019/)
[![Poster DOI](https://img.shields.io/badge/Poster%20DOI-10.6084%2Fm9.figshare.8082491.v1-informational.svg)](https://doi.org/10.6084/m9.figshare.8082491.v1)
[![Code DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2669160.svg)](https://doi.org/10.5281/zenodo.2669159)
[![Netlify Status](https://api.netlify.com/api/v1/badges/1662b8c4-ffb0-49a5-8331-a2f7370ed538/deploy-status)](https://app.netlify.com/sites/posters-lwjohnst/deploys)

# Explanation of project folders and files

This part of the README details how this research directory is structured, how
files should be run, and what the different files do. The layout and setup of
this project was designed for using [RStudio](https://www.rstudio.com/) and
[devtools](https://github.com/hadley/devtools). 

To install all the packages necessary for this project, run this command while
in the project:

    devtools::install_dev_deps()

Typical commands used in this workflow include:

- *Ctrl-Shift-B* when in RStudio or use `source("build.R")`

# General folder details

The project directory is generally structured with the following folders:

- Base folder (`./`)
- `R/` which contains all the R scripts and functions for running the analyses
and creating the content.
- `doc/` which contains the poster, abstract, and manuscript files.
- `data/` which contains the results dataframes and the (not always updated)
simulation dataset.
- `.git` 

The base folder contains a few files:

- `DESCRIPTION` is a standard file that includes metadata about your project, in
a machine readable format for others to obtain information on about your
project. It provides a description of what the project does and most importantly
what R packages your project uses on.
- The `.Rproj` file dictates that the directory is a RStudio project. Open the
project by opening this file.

All subsequent folders have their own README inside. See them for more details.

